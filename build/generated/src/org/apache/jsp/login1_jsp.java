package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login1_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form>\n");
      out.write("            <!-- Email input -->\n");
      out.write("            <div class=\"form-outline mb-4\">\n");
      out.write("                <input type=\"email\" id=\"form2Example1\" class=\"form-control\" />\n");
      out.write("                <label class=\"form-label\" for=\"form2Example1\">Email address</label>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <!-- Password input -->\n");
      out.write("            <div class=\"form-outline mb-4\">\n");
      out.write("                <input type=\"password\" id=\"form2Example2\" class=\"form-control\" />\n");
      out.write("                <label class=\"form-label\" for=\"form2Example2\">Password</label>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <!-- 2 column grid layout for inline styling -->\n");
      out.write("            <div class=\"row mb-4\">\n");
      out.write("                <div class=\"col d-flex justify-content-center\">\n");
      out.write("                    <!-- Checkbox -->\n");
      out.write("                    <div class=\"form-check\">\n");
      out.write("                        <input class=\"form-check-input\" type=\"checkbox\" value=\"\" id=\"form2Example31\" checked />\n");
      out.write("                        <label class=\"form-check-label\" for=\"form2Example31\"> Remember me </label>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"col\">\n");
      out.write("                    <!-- Simple link -->\n");
      out.write("                    <a href=\"#!\">Forgot password?</a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <!-- Submit button -->\n");
      out.write("            <button type=\"button\" class=\"btn btn-primary btn-block mb-4\">Sign in</button>\n");
      out.write("\n");
      out.write("            <!-- Register buttons -->\n");
      out.write("            <div class=\"text-center\">\n");
      out.write("                <p>Not a member? <a href=\"#!\">Register</a></p>\n");
      out.write("                <p>or sign up with:</p>\n");
      out.write("                <button type=\"button\" class=\"btn btn-link btn-floating mx-1\">\n");
      out.write("                    <i class=\"fab fa-facebook-f\"></i>\n");
      out.write("                </button>\n");
      out.write("\n");
      out.write("                <button type=\"button\" class=\"btn btn-link btn-floating mx-1\">\n");
      out.write("                    <i class=\"fab fa-google\"></i>\n");
      out.write("                </button>\n");
      out.write("\n");
      out.write("                <button type=\"button\" class=\"btn btn-link btn-floating mx-1\">\n");
      out.write("                    <i class=\"fab fa-twitter\"></i>\n");
      out.write("                </button>\n");
      out.write("\n");
      out.write("                <button type=\"button\" class=\"btn btn-link btn-floating mx-1\">\n");
      out.write("                    <i class=\"fab fa-github\"></i>\n");
      out.write("                </button>\n");
      out.write("            </div>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
