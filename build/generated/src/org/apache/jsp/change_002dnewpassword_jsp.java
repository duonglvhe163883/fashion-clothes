package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class change_002dnewpassword_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <!--Author: DPV-->\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"utf-8\" />\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\" />\n");
      out.write("        <meta name=\"description\" content=\"\" />\n");
      out.write("        <meta name=\"author\" content=\"\" />\n");
      out.write("        <title>QPS</title>\n");
      out.write("        <!-- Favicon-->\n");
      out.write("        <link rel=\"icon\" type=\"image/x-icon\" href=\"assets/q-icon.png\" />\n");
      out.write("        <!-- Bootstrap icons-->\n");
      out.write("        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css\" rel=\"stylesheet\" />\n");
      out.write("        <!-- Core theme CSS (includes Bootstrap)-->\n");
      out.write("        <link href=\"css/styles.css\" rel=\"stylesheet\" />\n");
      out.write("\n");
      out.write("        \n");
      out.write("    </head>\n");
      out.write("\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("        <section class=\"vh-100\" style=\"background-color: #eee;\">\n");
      out.write("            <div class=\"container h-100\">\n");
      out.write("                <div class=\"row d-flex justify-content-center align-items-center h-100\">\n");
      out.write("                    <div class=\"col-lg-12 col-xl-11\">\n");
      out.write("                        <div class=\"card text-black\" style=\"border-radius: 5px;\">\n");
      out.write("                            <div class=\"card-body p-md-5\">\n");
      out.write("                                <div class=\"row justify-content-center\">\n");
      out.write("                                    <div class=\"col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1\">\n");
      out.write("                                        <p class=\"text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4\">Change Password</p>\n");
      out.write("                                        <form class=\"mx-1 mx-md-4\" action=\"forgot-password?service=changePassword\" method=\"post\">\n");
      out.write("                                            <input type=\"text\" name=\"email\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${emailAddress}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" hidden=\"hidden\"> \n");
      out.write("                                            <div class=\"d-flex flex-row align-items-center mb-4\">\n");
      out.write("                                                <i class=\"fas fa-user fa-lg me-3 fa-fw\"></i>\n");
      out.write("                                                <div class=\"form-outline flex-fill mb-0\">\n");
      out.write("                                                    <label class=\"form-label\" for=\"form3Example1c\">New Password</label>\n");
      out.write("                                                    <input type=\"text\" id=\"form3Example1c\" class=\"form-control\" required name=\"newPassword\"/>\n");
      out.write("                                                </div>\n");
      out.write("                                            </div>\n");
      out.write("\n");
      out.write("                                            <div class=\"d-flex flex-row align-items-center mb-4\">\n");
      out.write("                                                <i class=\"fas fa-envelope fa-lg me-3 fa-fw\"></i>\n");
      out.write("                                                <div class=\"form-outline flex-fill mb-0\">\n");
      out.write("                                                    <label class=\"form-label\" for=\"form3Example3c\">Confirm Password</label>\n");
      out.write("                                                    <input type=\"text\" id=\"form3Example3c\" class=\"form-control\" required name=\"confirmPassword\"/>\n");
      out.write("                                                </div>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"d-block mx-4 mb-3 mb-lg-4  text-danger\"><b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${mess}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b></div>\n");
      out.write("                                            <div class=\"d-block mx-4 mb-3 mb-lg-4 text-center\">\n");
      out.write("                                                <button class=\"btn btn-primary btn-block px-5 w-100\" type=\"submit\">Confirm</button>\n");
      out.write("                                            </div>\n");
      out.write("                                            \n");
      out.write("                                            <div class=\"text-center pt-2\">\n");
      out.write("                                                <a href=\"../view/home\">Back to Home</a>\n");
      out.write("                                            </div>\n");
      out.write("                                        </form>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2\">\n");
      out.write("                                        <img src=\"https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp\"\n");
      out.write("                                             class=\"img-fluid\" alt=\"Sample image\">\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
